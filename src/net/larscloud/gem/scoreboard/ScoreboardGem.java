package net.larscloud.gem.scoreboard;

import net.larscloud.gem.Main;
import net.larscloud.gem.util.PlayerScore;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.ScoreboardManager;

public class ScoreboardGem implements Listener{

    private ScoreboardManager scoreboardManager;
    private Main instance;

    public ScoreboardGem(Main main) {
        this.scoreboardManager = Bukkit.getScoreboardManager();
        this.instance = main;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent playerJoinEvent) {
        // TODO: 19.04.16 Check if player is in map
        instance.playerMap.put(playerJoinEvent.getPlayer().getUniqueId().toString(),
                new PlayerScore(playerJoinEvent.getPlayer().getUniqueId().toString(), instance));
    }
}
