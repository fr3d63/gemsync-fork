package net.larscloud.gem.util;

import net.larscloud.gem.Main;
import net.larscloud.gem.database.MysqlManager;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.SQLException;
import java.sql.Statement;

public class Helper {
    private Main main;

    public Helper(Main main) {
        this.main = main;
        new BukkitRunnable() {
            @Override
            public void run() {
                if(MysqlManager.getInstance().connection == null) {
                    return;
                }
                try {
                    Statement s = MysqlManager.getInstance().getConnection().createStatement();
                    s.execute("CREATE TABLE IF NOT EXISTS Players(PLAYERID NCHAR(40), Coins INTEGER(255), PRIMARY KEY (PLAYERID))");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(main);
    }

}
