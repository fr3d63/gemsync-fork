package net.larscloud.gem.util;

import net.larscloud.gem.Main;
import net.larscloud.gem.database.MysqlManager;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PlayerScore {
    private int coins = -1;
    private String uuid;
    private Main instance;

    public PlayerScore(String uuid, Main main) {
        this.uuid = uuid;
        this.instance = main;
        put("INSERT IGNORE INTO Players(PLAYERID ,Coins) VALUES('" + uuid + "', 12312);");
        coinsFromDatabase();
    }

    public int getCoins() {
        return coins;
    }

    private void coinsFromDatabase() {
        new BukkitRunnable() {
            @Override
            public void run() {
                if (MysqlManager.getInstance().connection == null) {
                    return;
                }
                try {
                    Statement st = MysqlManager.getInstance().getConnection().createStatement();
                    st.execute("SELECT * FROM Players WHERE PLAYERID = '" + uuid + "';");
                    ResultSet rs = st.getResultSet();
                    if (rs.next())
                        coins = rs.getInt("Coins");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(instance);

    }

    public void put(String statement) {
        new BukkitRunnable() {
            @Override
            public void run() {
                if (MysqlManager.getInstance().connection == null) {
                    return;
                }
                try {
                    Statement s = MysqlManager.getInstance().getConnection().createStatement();
                    s.execute(statement);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(instance);
    }
}