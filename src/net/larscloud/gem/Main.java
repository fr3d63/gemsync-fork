package net.larscloud.gem;

import net.larscloud.gem.scoreboard.ScoreboardGem;
import net.larscloud.gem.util.Helper;
import net.larscloud.gem.util.PlayerScore;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;


public class Main extends JavaPlugin {
    public static Helper hp;
    private PluginManager pluginManager = this.getServer().getPluginManager();
    public Map<String, PlayerScore> playerMap;

    @Override
    public void onEnable() {
        this.playerMap = new HashMap<>();
        hp = new Helper(this);
        //hp.put("INSERT INTO Players(UUID, GEMS)VALUE ("+ uuid +", 1)");
        //hp.put("INSERT INTO Players(UUID, GEMS)VALUE ("+ uuid2 +", 111123)");
        pluginManager.registerEvents(new ScoreboardGem(this), this);

    }

    @Override
    public void onDisable() {
    }
}
