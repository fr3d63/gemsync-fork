package net.larscloud.gem.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlManager {
    private static MysqlManager instance;
    public Connection connection;
    private final String host;
    private final String db;
    private final String user;
    private final String pw;

    private MysqlManager() {
        host = "192.168.10.10";
        db = "homestead";
        user = "homestead";
        pw = "secret";

        try {
            openConnection();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static MysqlManager getInstance() {
        if (instance == null) {
            instance = new MysqlManager();
        }

        return instance;
    }

    private void openConnection() throws SQLException, ClassNotFoundException {
        if (connection != null && !connection.isClosed()) {
            return;
        }

        synchronized (this) {
            if (connection != null && !connection.isClosed()) {
                return;
            }
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + 3306 + "/" + this.db, this.user, this.pw);
        }
    }

    public Connection getConnection() {
        return connection;
    }

}
